"use strict";

import requireDir from 'require-dir'
import gulpUtil from 'gulp-util'

global.devBuild = gulpUtil.env.env !== 'production';

requireDir('./gulp/tasks', { recurse: true });