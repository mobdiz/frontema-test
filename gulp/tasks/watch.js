"use strict";

import gulp from 'gulp'

import config from '../config'

gulp.task('watch', ['build'], () => {
    gulp.watch(config.scss.paths.watch,   ['scss']);
    gulp.watch(config.images.paths.watch, ['images']);
    gulp.watch(config.pug.paths.watch,    ['pug']);
    gulp.watch(config.locals.paths.watch, ['pug']);
    gulp.watch(config.pkg.paths.watch,    ['pug']);
});
