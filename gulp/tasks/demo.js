"use strict";

import gulp from 'gulp'
import config from '../config'
import browserSync from 'browser-sync';

global.liveReload = browserSync.create();

gulp.task('demo', ['watch'], () => {
    liveReload.init(config.demo.browserSync);
});
