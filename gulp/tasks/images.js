'use strict';

import gulp from 'gulp'
import gulpIf from 'gulp-if'
import gulpImageMin from 'gulp-imagemin'

import imageMinJpegReCompress from 'imagemin-jpeg-recompress'
import pngQuant from 'imagemin-pngquant'

import config from '../config'
import rename from 'gulp-rename'

gulp.task('images', () =>
    gulp.src(config.images.paths.src)
        .pipe(rename(item => {
            item.dirname = item.dirname.replace('/images/', '/');
        }))
        .pipe(gulpIf(!devBuild, gulpImageMin([
            gulpImageMin.gifsicle({
                interlaced: true
            }),
            gulpImageMin.jpegtran({
                progressive: true
            }),
            imageMinJpegReCompress({
                loops: 5,
                min: 65,
                max: 70,
                quality: 'medium'
            }),
            gulpImageMin.svgo({
                plugins: [{
                    removeViewBox: false
                }]
            }),
            gulpImageMin.optipng({
                optimizationLevel: 3
            }),
            pngQuant({
                quality: '65-70',
                speed: 5
            })
        ], {
            verbose: true
        })))
        .pipe(gulpIf(devBuild, liveReload.stream()))
        .pipe(gulp.dest(config.images.paths.dest))
);
