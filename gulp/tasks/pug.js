"use strict";

import gulp from "gulp"
import pug from "gulp-pug"
import data from "gulp-data"
import rename from "gulp-rename"
import postHtml from "gulp-posthtml"
import postHtmlBemPlugin from "posthtml-bem"

import config from "../config"
import getLocals from "../helpers/get-locals";

let postHtmlPlugins = [
    postHtmlBemPlugin({
        elemPrefix: "__",
        modPrefix: "--",
        modDlmtr: "_"
    })
];

gulp.task("pug", () =>
    gulp.src(config.pug.paths.src)
        .pipe(data(getLocals()))
        .pipe(pug(config.pug.settings))
        .pipe(postHtml(postHtmlPlugins))
        .pipe(rename(config.pug.renameFunc))
        .pipe(gulp.dest(config.pug.paths.dest))
        .on('end', () => {
            if (devBuild) {
                liveReload.reload();
            }
        })
);
