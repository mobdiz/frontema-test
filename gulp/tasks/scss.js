"use strict";

import gulp from 'gulp'
import sassGlob from 'gulp-sass-glob'
import sass from 'gulp-sass'
import prefix from 'gulp-autoprefixer'
import csso from 'gulp-csso'
import gulpIf from 'gulp-if'
import concat from 'gulp-concat'
import sourceMaps from 'gulp-sourcemaps'

import config from '../config'

gulp.task('scss', () =>
    gulp.src(config.scss.paths.src)
        .pipe(sourceMaps.init())
        .pipe(sassGlob())
        .pipe(sass(config.scss.settings))
        .pipe(prefix(config.scss.autoprefixer))
        .pipe(concat(config.scss.filename))
        .pipe(gulpIf(!devBuild, csso(config.scss.compress)))
        .pipe(sourceMaps.write())
        .pipe(gulp.dest(config.scss.paths.dest))
        .pipe(gulpIf(devBuild, liveReload.stream()))
);
