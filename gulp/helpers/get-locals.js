import fs from 'fs'
import config from "../config";

export default () => {
    let types = config.locals.paths.types;

    let data = Object.keys(types).reduce((result, type) => {
        result[type] = fs.readdirSync(types[type]).reduce((data, page) => {
            let path = types[type] + page;
            let isDirectory = fs.statSync(path).isDirectory();

            if (isDirectory) {
                let filePath = path + '/' + page + '.json';
                let isExist = fs.existsSync(filePath);

                if (isExist) {
                    let fileData = fs.readFileSync(filePath, 'utf8');

                    data[page] = JSON.parse(fileData);
                }
            }

            return data;
        }, {});

        return result;
    }, {});

    let pkg = JSON.parse(fs.readFileSync(config.pkg.paths.src, 'utf8'));

    data.pkgVersion = pkg.version;

    return data;
}
