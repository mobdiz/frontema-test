"use strict";

const _src  = './src/';
const _dist = './dist/';

const _pages  = 'pages/';
const _blocks = 'blocks/';
const _scss   = 'scss/';
const _assets = 'assets/';
const _css    = 'css/';
const _images = 'images/';

const _pkg = './package.json';

export default {
    pug: {
        paths: {
            src: _src + _pages + "*/*.pug",
            dest: _dist,
            watch: _src + "**/*.pug"
        },
        settings: {
            pretty: devBuild
        },
        renameFunc: (path) => {
            path.dirname = './';
        }
    },
    scss: {
        paths: {
            src: _src + _scss + "*.scss",
            dest: _dist + _assets + _css,
            watch: _src + "**/*.scss"
        },
        filename: 'style.css',
        params: {
            compress: false
        },
        autoprefixer: {
            browsers: ['> 0.25%', 'last 2 versions'],
            cascade: false
        },
        compress: {},
        settings: {
            includePaths : [
                _src
            ]
        }
    },
    images: {
        paths: {
            src: _src + '**/*.+(jpg|png|svg)',
            dest: _dist + _assets + _images,
            watch: _src + '**/*.+(jpg|png|svg)'
        }
    },
    locals: {
        paths: {
            types: {
                pages: _src + _pages,
                blocks: _src + _blocks,
            },
            watch: _src + '**/*.json'
        }
    },
    pkg: {
        paths: {
            src: _pkg,
            watch: _pkg
        }
    },
    clear: _dist,
    demo: {
        browserSync: {
            server: {
                baseDir: _dist
            }
        }
    }
}
